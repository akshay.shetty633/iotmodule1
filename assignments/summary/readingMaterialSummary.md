**INDUSTRAIL IOT**

IIoT stands for the Industrial Internet of Things or Industrial IoT that initially mainly referred to an industrial framework whereby a large number of devices or machines are connected and synchronized through the use of software tools and third platform technologies in a machine-to-machine and Internet of Things context, later an Industry 4.0 or Industrial Internet context.

**Different Industrial IOT:**
- HOME IOT
- GARAGE IOT
- BUSINESS IOT



**Industrial revolution:**

History

**1st Industrial Revolution**
The First Industrial Revolution began in the 18th century through the use of steam power and mechanisation of production. What before produced threads on simple spinning wheels, the mechanised version achieved eight times the volume in the same time. Steam power was already known.  Instead of weaving looms powered by muscle, steam-engines could be used for power. 

 

**2nd Industrial Revolution**
The Second Industrial Revolution began in the 19th century through the discovery of electricity and assembly line production. Henry Ford (1863-1947) took the idea of mass production from a slaughterhouse in Chicago: . While before one station assembled an entire automobile, now the vehicles were produced in partial steps on the conveyor belt - significantly faster and at lower cost.

 

**3rd Industrial Revolution**

The Third Industrial Revolution began in the ’70s in the 20th century through partial automation using memory-programmable controls and computers. Since the introduction of these technologies, we are now able to automate an entire production process - without human assistance. Known examples of this are robots that perform programmed sequences without human intervention.

 

**4th Industrial Revolution**
We are currently implementing the Fourth Industrial Revolution. This is characterised by the application of information and communication technologies to industry and is also known as "Industry 4.0". It builds on the developments of the Third Industrial Revolution. Production systems that already have computer technology are expanded by a network connection and have a digital twin on the Internet so to speak. These allow communication with other facilities and the output of information about themselves. This is the next step in production automation. The networking of all systems leads to "cyber-physical production systems" and therefore smart factories, in which production systems, components and people communicate via a network and production is nearly autonomous.

**Stages of industrial revolution:**


![ALT](https://www.beca.com/getmedia/26aff11f-f71b-4195-a4c7-d482f6314a0e/Industry-4-diagram.png/)


**IOT Architecture:**

![ALT](https://miro.medium.com/max/875/1*FwH9u2aV-m8nEbnSB7ffGg.png)

**IOT 3.0:**

Data is stored in databases and represented in excels

Sensors,PLC's,SCADA & ERP:

Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data
Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts.

**Industry 3.0 protocols:**

All these protocols are optimized for sending data to a central server inside the factory.
These protocols are used by sensors to Send data to PLC's. These protocols are called as fieldbus.

![ALT](http://www.icpdas.com/root/news/products/images/2016/022501_01.gif)

**IOT 3.0 Architecture**


*Field devices*

- sensors
- acutators
- Motors


*Control devices*

- Microcontrollers
- Computer Numeric Controls

*Stations*

- Enterprises
- The field bus connects the sensors and controllers.


**IOT 4.0**

![ALT](https://base.imgix.net/files/base/ebm/electronicdesign/image/2019/12/Mathworks_Promo_Industry_4.0___938674002.5e0630a8da255.png?auto=format&fit=crop&h=432&w=768)

Greater flexibility, virtual validation, industrial controllers, and edge computing are some of the hot topics and trends that will likely dominate Industry 4.0 going forward.

**IOT Protocols:**

![ALT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ2o3A0OHW9pkOxJXQh3Sa2uryHJmTKhAaZIQ&usqp=CAU)

**Protocols:**

MQTT
Web Sockets
https
REST API

**Problem Industry owners face:**


Cost:
Industry 3.0 devices are very Expensive as a factory owner I dont want to switch to Industry 4.0 devices because they are Expensive.

Downtime:
Changing Hardware means a big factory downtime, I dont want to shut down my factory to upgrade devices.

Reliablity:
I dont want to invest in devices which are unproven and unreliable.

IOT 3.0 to 4.0:

![ALT](https://gitlab.com/Priyavarshini/iotmodule1/uploads/d5b8fbda8943cc6600e7ff8adcdc8609/Interaction-between-hierarchies-Levels-in-Industry-30-and-Industry-40-source.png)

**Difference between Industry 3.0 and Industry 4.0:**

In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor.

For an example of the old way (Industry 3.0), take a CNC machine: while largely automated, it still needs input from a human controller. The process is automated based on human input, not by data. Under Industry 4.0, that same CNC machine would not only be able to follow set programming parameters, but also use data to streamline production processes.

**Conversion of IOT 3.0 to 4.0:**

![ALT](https://gitlab.com/akshaykpanchal/iotmodule1/-/raw/master/extras/7.png)

**Data stored in IOT platform's:**

- AWS IOT
- Google IoT
- Azure IoT
- Thingsboard


**Get Alerts based on your data using these platforms:**

Zaiper
Twilio

